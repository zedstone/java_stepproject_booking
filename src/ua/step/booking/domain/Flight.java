package ua.step.booking.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Flight implements Serializable {
  static int counter=1;

  private int id;

  private String flightFrom;
  private String flightTo;

  private LocalDate date;
  private int availableSeats;

  public Flight(String flightFrom, String flightTo, LocalDate date, int availableSeats) {
    this.id = counter++;
    this.flightFrom = flightFrom;
    this.flightTo = flightTo;
    this.date = date;
    this.availableSeats = availableSeats;
  }

  public int getId() {
    return id;
  }

  public String getFlightFrom() {
    return flightFrom;
  }

  public void setFlightFrom(String flightFrom) {
    this.flightFrom = flightFrom;
  }

  public String getFlightTo() {
    return flightTo;
  }

  public void setFlightTo(String flightTo) {
    this.flightTo = flightTo;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public int getAvailableSeats() {
    return availableSeats;
  }

  public void setAvailableSeats(int availableSeats) {
    this.availableSeats = availableSeats;
  }

  @Override
  public String toString() {
    return "Flight{" +
      "id=" + id +
      ", flightFrom='" + flightFrom + '\'' +
      ", flightTo='" + flightTo + '\'' +
      ", date=" + date +
      ", availableSeats=" + availableSeats +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Flight flight = (Flight) o;
    return id == flight.id && flightFrom.equals(flight.flightFrom) && flightTo.equals(flight.flightTo) && date.equals(flight.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, flightFrom, flightTo, date);
  }
}
