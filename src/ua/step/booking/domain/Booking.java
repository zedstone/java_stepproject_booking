package ua.step.booking.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

public class Booking implements Serializable {

  static int counter=1;

  private int id;

  private String flightFrom;
  private String flightTo;

  private LocalDate date;
  private String[] passengers;

  public Booking(String flightFrom, String flightTo, LocalDate date, String[] passengers) {
    this.id = counter++;
    this.flightFrom = flightFrom;
    this.flightTo = flightTo;
    this.date = date;
    this.passengers = passengers;
  }

  public String getFlightFrom() {
    return flightFrom;
  }

  public void setFlightFrom(String flightFrom) {
    this.flightFrom = flightFrom;
  }

  public String getFlightTo() {
    return flightTo;
  }

  public void setFlightTo(String flightTo) {
    this.flightTo = flightTo;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String[] getPassengers() {
    return passengers;
  }

  public void setPassengers(String[] passengers) {
    this.passengers = passengers;
  }

  public int getId() {
    return id;
  }

  @Override
  public String toString() {
    return "Booking{" +
      "id = '" + id + '\'' +
      " flightFrom='" + flightFrom + '\'' +
      ", flightTo='" + flightTo + '\'' +
      ", date=" + date +
      ", passengers=" + Arrays.toString(passengers) +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Booking booking = (Booking) o;
    return id == booking.id && Arrays.equals(passengers, booking.passengers);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(id);
    result = 31 * result + Arrays.hashCode(passengers);
    return result;
  }
}
