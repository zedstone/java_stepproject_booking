package ua.step.booking;

import ua.step.booking.controllers.BookingController;
import ua.step.booking.controllers.FlightController;
import ua.step.booking.domain.Flight;
import ua.step.booking.excep.OutOfRange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    FlightController flightController = new FlightController();
    BookingController bookingController = new BookingController();
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    int userEnter = 0;

    while (true){
      System.out.println("make a choice");
      System.out.println("1. Show All flights");
      System.out.println("2. View flight information");
      System.out.println("3. Flight search and booking");
      System.out.println("4. Cancel booking");
      System.out.println("5. Search booking");
      System.out.println("6. booking all");
      System.out.println("7. Clear All flights");
      System.out.println("8. Exit");

      try {
        userEnter = Integer.parseInt(br.readLine());
      } catch (IOException e) {
        e.printStackTrace();
      }

      if(userEnter == 1){
        flightController.showAllFlights();
      }
      if(userEnter == 2){
        while (true){
          System.out.println("Enter flight id");
          try {
            userEnter = Integer.parseInt(br.readLine());
            if (flightController.flightInfo(userEnter) == null){
              throw new OutOfRange();
            }
            System.out.println(flightController.flightInfo(userEnter));
            break;
          }catch (OutOfRange | NumberFormatException e) {
            System.out.println("Exception: "+e.toString());
          } catch
          (IOException e) {
            e.printStackTrace();
          }
        }

      }if(userEnter == 3){
        String userDestination = "";
        String userEnterDate = "";
        LocalDate userDate = LocalDate.now();
        int userSeats = 0;
        List<Flight> searchList = null;

        while (true){
          System.out.println("Where do you want to fly?");
          try {
            userDestination = br.readLine();
          } catch
          (IOException e) {
            e.printStackTrace();
          }

          System.out.println("Enter the date in format: 2022-03-25");
          try {
            userEnterDate = br.readLine();
            userDate = LocalDate.parse(userEnterDate);
          } catch
          (IOException e) {
            e.printStackTrace();
          }catch (DateTimeParseException e){
            System.out.println("Error date format");
            continue;
          }
          System.out.println("Enter the number of seats");
          try {
            userSeats = Integer.parseInt(br.readLine());
            if (userSeats <= 0){
              throw new OutOfRange();
            }
          }catch (NumberFormatException e){
            System.out.println("Error. Enter the number");
            continue;
          }
          catch (OutOfRange e) {
            System.out.println("Exception: "+e.toString());
            continue;
          } catch (IOException e) {
            e.printStackTrace();
          }
          searchList = flightController.searchFlights(userDestination, userDate.getYear(), userDate.getMonthValue(), userDate.getDayOfMonth(), userSeats);
          if(searchList.size() == 0){
            System.out.println("No flights found");
          }else {
            searchList.forEach(System.out::println);
          }

          break;
        }
        while (true){
          System.out.println("1. Booking the flight");
          System.out.println("2. To main menu");

          try {
            userEnter = Integer.parseInt(br.readLine());
          }catch (NumberFormatException e){
            System.out.println("Error. Enter the number");
            continue;
          } catch (IOException e) {
            e.printStackTrace();
          }
          Flight bookingFlight = null;

          if(userEnter == 1){
            System.out.println("Enter flight id");
            try {
              userEnter = Integer.parseInt(br.readLine());
              if (flightController.flightInfo(userEnter) == null) {
                throw new OutOfRange();
              }

            }catch (OutOfRange | NumberFormatException e) {
              System.out.println("Exception: "+e.toString());
            } catch
            (IOException e) {
              e.printStackTrace();
            }
            String[] passengerList = new String[userSeats];

            for (int i = 0; i < userSeats; i++) {
              String passenger = "";
              try {
                System.out.printf("Enter the first name and last name of the passenger № %d %n" , i + 1);
                passenger = br.readLine();
              } catch (IOException e) {
                e.printStackTrace();
              }
              passengerList[i] = passenger;
            }

            bookingFlight = flightController.flightInfo(userEnter);

            flightController.updateFlightSeats(bookingFlight.getId(), bookingFlight.getAvailableSeats() - userSeats);

            System.out.println(bookingFlight);
            System.out.println("booking!");

            bookingController.createBooking(bookingFlight.getFlightFrom(), bookingFlight.getFlightTo(), bookingFlight.getDate(), passengerList);
            break;
          }

          if(userEnter == 2){
            break;
          }
        }

      }
      if(userEnter == 4){
        bookingController.printBooking(bookingController.readBookings());
        while (true){
          System.out.println("Enter booking index for cancel");
          try {
            userEnter = Integer.parseInt(br.readLine());
            if (bookingController.deleteBooking(userEnter)){
              System.out.println("Booking №" + userEnter + " deleted");
            }else {
              System.out.println("No such booking number");
            }
            break;
          }catch (NumberFormatException e) {
            System.out.println("Exception: "+e.toString());
          }catch (IndexOutOfBoundsException e){
            System.out.println("out of range");
          }
          catch(IOException e) {
            e.printStackTrace();
          }
        }
      }
      if(userEnter == 5){
       String userFirstName = "";
       String userLastName = "";

        while (true){
          System.out.println("Enter first name");
          try {
            userFirstName = br.readLine();
          } catch
          (IOException e) {
            e.printStackTrace();
          }

          System.out.println("Enter last name");
          try {
            userLastName = br.readLine();
          } catch
          (IOException e) {
            e.printStackTrace();
          }
          bookingController.showMyBooking(userFirstName, userLastName);
          break;
       }

        while (true){

          try {
            System.out.println("Enter index for cancel booking");
            System.out.println("Enter 0 to main menu");

            userEnter = Integer.parseInt(br.readLine());
            br.readLine();
          }catch (NumberFormatException e) {
            System.out.println("Exception: "+e.toString());
          }catch (IndexOutOfBoundsException e){
            System.out.println("out of range");
          }
          catch(IOException e) {
            e.printStackTrace();
          }
          if (userEnter == 0){
            break;
          }

            if (bookingController.deleteBooking(userEnter)){
              System.out.println("Booking №" + userEnter + " deleted");
            }else {
              System.out.println("No such booking number");
            }
            break;

        }

      }

      if(userEnter == 6){
        System.out.println(bookingController.readBookings());
      }

      if(userEnter == 7){
        flightController.clearDB();
      }
      if(userEnter == 8){
        break;
      }
    }

  }
}
