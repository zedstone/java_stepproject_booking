package ua.step.booking.services;

import ua.step.booking.domain.Booking;

import java.time.LocalDate;
import java.util.List;

public interface BookingServiceInt {
  List<Booking> readBookings();

  void createBooking(String flightFrom, String flightTo, LocalDate date, String[] passengers);

  void showMyBooking(String surName, String name);

  Booking BookingInfo(int id);

  boolean deleteBooking(int id);

  void printBooking(List<Booking> bookings);
}
