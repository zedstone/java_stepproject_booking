package ua.step.booking.services;

import ua.step.booking.domain.Flight;

import java.util.List;

public interface FlightServiceInt {

  void createFlight(String from, String to, int yearDate, int monthDate, int dayOfMonthDate, int seats);

  List<Flight> readFlights();

  void updateFlightSeats(int id, int newSeats);

  void showAllFlights();

  Flight flightInfo(int id);

  List<Flight> searchFlights(String destination, int yearDate, int monthDate, int dayOfMonthDate, int amount);

  void clearDB();

  void deleteFlight(int id);

}
