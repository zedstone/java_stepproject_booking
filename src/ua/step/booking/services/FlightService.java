package ua.step.booking.services;


import ua.step.booking.dao.FlightDao;
import ua.step.booking.dao.FlightDaoInt;
import ua.step.booking.domain.Flight;

import java.util.List;

public class FlightService implements FlightServiceInt{
  private FlightDaoInt flightDao = new FlightDao();

  @Override
  public void createFlight(String from, String to, int yearDate, int monthDate, int dayOfMonthDate, int seats){
    flightDao.createFlight(from, to, yearDate, monthDate, dayOfMonthDate, seats);
  }

  @Override
  public List<Flight> readFlights() {
    return flightDao.readFlights();
  }

  @Override
  public void updateFlightSeats(int id, int newSeats) {
    flightDao.updateFlightSeats(id, newSeats);
  }

  @Override
  public void showAllFlights() {
    flightDao.showAllFlights();
  }

  @Override
  public Flight flightInfo(int id) {
    return flightDao.flightInfo(id);
  }

  @Override
  public List<Flight> searchFlights(String destination, int yearDate, int monthDate, int dayOfMonthDate, int amount) {
    return flightDao.searchFlights(destination, yearDate, monthDate, dayOfMonthDate, amount);
  }

  @Override
  public void clearDB() {
    flightDao.clearDB();
  }

  @Override
  public void deleteFlight(int id) {
    flightDao.deleteFlight(id);
  }

}
