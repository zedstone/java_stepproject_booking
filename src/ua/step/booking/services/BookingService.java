package ua.step.booking.services;

import ua.step.booking.dao.BookingDao;
import ua.step.booking.dao.BookingDaoInt;
import ua.step.booking.domain.Booking;

import java.time.LocalDate;
import java.util.List;

public class BookingService implements BookingServiceInt{
  private BookingDaoInt bookingDao = new BookingDao();

  @Override
  public List<Booking> readBookings() {
    return bookingDao.readBookings();
  }

  @Override
  public void createBooking(String flightFrom, String flightTo, LocalDate date, String[] passengers) {
    bookingDao.createBooking(flightFrom, flightTo, date, passengers);
  }

  @Override
  public void showMyBooking(String surName, String name) {
    bookingDao.showMyBooking(surName, name);
  }

  @Override
  public Booking BookingInfo(int id) {
    return null;
  }

  @Override
  public boolean deleteBooking(int id) {
    return bookingDao.deleteBooking(id);
  }

  @Override
  public void printBooking(List<Booking> bookings) {
    bookingDao.printBooking(bookings);
  }
}
