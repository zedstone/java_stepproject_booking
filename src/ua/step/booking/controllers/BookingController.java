package ua.step.booking.controllers;

import ua.step.booking.domain.Booking;
import ua.step.booking.services.BookingService;
import ua.step.booking.services.BookingServiceInt;

import java.time.LocalDate;
import java.util.List;

public class BookingController {
  private BookingServiceInt bookingService = new BookingService();


  public List<Booking> readBookings() {
    return bookingService.readBookings();
  }


  public void createBooking(String flightFrom, String flightTo, LocalDate date, String[] passengers) {
    bookingService.createBooking(flightFrom, flightTo, date, passengers);
  }


  public void showMyBooking(String surName, String name) {
    bookingService.showMyBooking(surName, name);
  }


  public Booking BookingInfo(int id) {
    return null;
  }

  public boolean deleteBooking(int id) {
    return bookingService.deleteBooking(id);
  }
  public void printBooking(List<Booking> bookings) {
    bookingService.printBooking(bookings);
  }
}
