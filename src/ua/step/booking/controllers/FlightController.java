package ua.step.booking.controllers;

import ua.step.booking.domain.Flight;
import ua.step.booking.services.FlightService;
import ua.step.booking.services.FlightServiceInt;

import java.util.List;

public class FlightController {
  private FlightServiceInt flightService = new FlightService();

  public void createFlight(String from, String to, int yearDate, int monthDate, int dayOfMonthDate, int seats){
    flightService.createFlight(from, to, yearDate, monthDate, dayOfMonthDate, seats);
  }

  public void print(){
    System.out.println("hay");
  }

  public List<Flight> readFlights() {
    return flightService.readFlights();
  }
  public void showAllFlights() {
    flightService.showAllFlights();
  }
  public void clearDB() {
    flightService.clearDB();
  }

  public Flight flightInfo(int id) {
    return flightService.flightInfo(id);
  }
  public List<Flight> searchFlights(String destination, int yearDate, int monthDate, int dayOfMonthDate, int amount) {
    return flightService.searchFlights(destination, yearDate, monthDate, dayOfMonthDate, amount);
  }
  public void deleteFlight(int id) {
    flightService.deleteFlight(id);
  }
  public void updateFlightSeats(int id, int newSeats) {
    flightService.updateFlightSeats(id, newSeats);
  }

}


