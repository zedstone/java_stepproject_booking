package ua.step.booking.dao;

import ua.step.booking.domain.Flight;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class FlightDao implements FlightDaoInt{
  private final String DATA_FILE_NAME = "src/ua/step/booking/data/data.txt";
  private List<Flight> flights;

  private File file;

  public FlightDao() {
    this.file =new File(DATA_FILE_NAME);
    if(!file.exists()){
      try {
        file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if(flights == null){
      flights = new ArrayList<>();
    }
    if (flights.size() < 30){
      randomGenerateDB();//Ми не знаємо, коли будуть відкривати наш проект, тому генеруємо базу взалежності від дати запуску
    }
    flights = readFlights();

  }

  @Override
  public void createFlight(String from, String to, int yearDate, int monthDate, int dayOfMonthDate, int seats){
    LocalDate currentDate = LocalDate.of(yearDate, monthDate, dayOfMonthDate);
    Flight newFlight = new Flight(from, to, currentDate, seats);
    flights.add(newFlight);

    try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DATA_FILE_NAME))) {
      oos.writeObject(flights);
    }catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void deleteFlight(int id){

      flights = flights.stream().filter(f->f.getId() !=id).toList();

    try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DATA_FILE_NAME))) {
      oos.writeObject(flights);
    }catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void updateFlightSeats(int id, int newSeats){

    Flight updateFlight = flights.stream().filter(u->u.getId() == id).findFirst().get();
    updateFlight.setAvailableSeats(newSeats);

    //flights = flights.stream().filter(f->f.getId() !=id).toList();

    flights = flights.stream()
      .map(o -> {
        if(o.getId() == id) {
          return updateFlight;
        } else {
          return o;
        }
      }).toList();

    try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DATA_FILE_NAME))) {
      oos.writeObject(flights);
    }catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public List<Flight> readFlights(){
    try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(DATA_FILE_NAME))) {
      return (List<Flight>) ois.readObject();
    }catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
      return new ArrayList<>();
    }
  }

  @Override
  public void showAllFlights(){
    flights.forEach(System.out::println);
  }
  @Override
  public Flight flightInfo(int id){
    return flights.stream().filter(f-> f.getId() == id).findFirst().orElse(null);
  }

  @Override
  public void randomGenerateDB(){

    for (int i = 0; i < 40; i++) {
      Random random = new Random();

      LocalDate currentDate = LocalDate.now();

      int freeSeats = random.nextInt(0,10);
      int flyToRandom = random.nextInt(0,7);
      String flyTo;

      if(flyToRandom == 0){
        flyTo = "rome";
      }else if(flyToRandom == 1){
        flyTo = "madrid";
      }else if(flyToRandom == 2){
        flyTo = "london";
      }else if(flyToRandom == 3){
        flyTo = "paris";
      }else if(flyToRandom == 4){
        flyTo = "tokyo";
      }else if(flyToRandom == 5){
        flyTo = "chicago";
      }else {
        flyTo = "warsaw";
      }

      createFlight("kyiv", flyTo, currentDate.getYear(), currentDate.getMonthValue(), currentDate.getDayOfMonth(), freeSeats);
    }
  }

  @Override
  public List<Flight> searchFlights(String destination, int yearDate, int monthDate, int dayOfMonthDate, int amount){

    LocalDate userDate = LocalDate.of(yearDate, monthDate, dayOfMonthDate);
    List<Flight> newList = flights.stream().
      filter(f-> Objects.equals(f.getFlightTo(), destination))
      .filter(f -> f.getAvailableSeats() >= amount)
      .filter(f -> f.getDate().compareTo(userDate) == 0)
      .toList();
    return newList;
  }

  @Override  //Щоб була можливість чистити базу
  public void clearDB(){
    flights.clear();
    try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DATA_FILE_NAME))) {
      oos.writeObject(flights);
    }catch (IOException e) {
      e.printStackTrace();
    }
  }


}
