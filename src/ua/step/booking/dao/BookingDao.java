package ua.step.booking.dao;

import ua.step.booking.domain.Booking;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class BookingDao implements BookingDaoInt{
  private final String DATA_FILE_NAME = "src/ua/step/booking/data/data_booking.txt";
  private List<Booking> bookings;



  private File file;

  public BookingDao() {
    this.file =new File(DATA_FILE_NAME);
    if(!file.exists()){
      try {
        file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if(bookings == null){
      bookings = new ArrayList<>();
    }
    bookings = readBookings();
  }

  @Override
  public void loadDataBooking(List<Booking> bookingList){

    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DATA_FILE_NAME))){
      oos.writeObject(bookingList);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  @Override
  public List<Booking> readDataBooking(){
    try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(DATA_FILE_NAME))){
      return (List<Booking>) ois.readObject();

    }catch(EOFException e) {
      return new ArrayList<>();
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
      return new ArrayList<>();
    }

  }

//  @Override
//  public List<Booking> readBookings(){
//    List<Booking> bookingList = new ArrayList<>();
//    try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(DATA_FILE_NAME))) {
//      bookingList = (List<Booking>) ois.readObject();
//      if (ois.readObject() == null){
//        return new ArrayList<>();
//      }else {
//        return bookingList;
//      }
//
//    }catch (IOException | ClassNotFoundException e) {
//      return bookingList;
//    }
//  }

//  @Override
//  public void createBooking(String flightFrom, String flightTo, LocalDate date, String[] passengers){
//
//    Booking newBooking = new Booking(flightFrom, flightTo, date, passengers);
//    bookings = new ArrayList<>(bookings.size() + 1);
//    bookings.add(newBooking);
//
//    try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DATA_FILE_NAME))) {
//      oos.writeObject(bookings);
//    }catch (UnsupportedOperationException e){
//      e.getMessage();
//    }
//    catch (IOException e) {
//      e.printStackTrace();
//    }
//
//  }
  @Override
  public List<Booking> readBookings(){
    List<Booking> bookings = readDataBooking();

    return new ArrayList<>(bookings);
  }

  @Override
  public void createBooking(String flightFrom, String flightTo, LocalDate date, String[] passengers){

    Booking newBooking = new Booking(flightFrom, flightTo, date, passengers);

    try{
      bookings.add(newBooking);
    }catch (UnsupportedOperationException e){
      System.out.println(e.getMessage());
    }

    loadDataBooking(bookings);

  }
  @Override
  public void printBooking(List<Booking> bookings){
//    for (int i = 0; i < bookings.size(); i++) {
//      System.out.println(bookings.get(i));
//    }
    bookings.stream().forEach(System.out::println);
  }

  @Override
  public void showMyBooking(String name, String surName){
    String userEnterName = (name + " " + surName).toLowerCase(Locale.ROOT);
    List<Booking> bookingsFiltered;
    bookingsFiltered = bookings.stream().filter(b -> {

     String str = Arrays.stream(b.getPassengers()).filter(pas -> pas.toLowerCase(Locale.ROOT).equals(userEnterName))
        .findAny()
        .orElse(null);
    if(str != null && str.length() > 0){
      return true;
    }else {
      return false;
    }
    }).toList();
    printBooking(bookingsFiltered);
  }
  @Override
  public Booking BookingInfo(int id){
    return bookings.stream().filter(f-> f.getId() == id).findFirst().orElse(null);
  }

  @Override
  public boolean deleteBooking(int id){
    int startSize = bookings.size();
    bookings = bookings.stream().filter(b -> b.getId() !=id).toList();
    int endSize = bookings.size();

    try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DATA_FILE_NAME))) {
      oos.writeObject(bookings);
      if (startSize > endSize){
        return true;
      }else {
        return false;
      }
    }catch (IOException e) {
      e.printStackTrace();
      return false;
    }
  }
}
