package ua.step.booking.dao;

import ua.step.booking.domain.Flight;

import java.util.List;

public interface FlightDaoInt {
  void createFlight(String from, String to, int yearDate, int monthDate, int dayOfMonthDate, int seats);

  void deleteFlight(int id);

  void updateFlightSeats(int id, int newSeats);

  List<Flight> readFlights();

  void showAllFlights();

  Flight flightInfo(int id);

  void randomGenerateDB();

  List<Flight> searchFlights(String destination, int yearDate, int monthDate, int dayOfMonthDate, int amount);

  void clearDB();
}
