package ua.step.booking.dao;

import ua.step.booking.domain.Booking;
import ua.step.booking.domain.Flight;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public interface BookingDaoInt {
  void loadDataBooking(List<Booking> bookingList);

  List<Booking> readDataBooking();

  List<Booking> readBookings();

  void createBooking(String flightFrom, String flightTo, LocalDate date, String[] passengers);

  void printBooking(List<Booking> bookings);

  void showMyBooking(String surName, String name);

  Booking BookingInfo(int id);

  boolean deleteBooking(int id);
}
